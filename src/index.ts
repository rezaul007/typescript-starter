/** generics function */

const arrayReverse = <T> (arr: T[]): T[] => {
    console.log(arr.length)
    return arr.reverse()
}

console.log("generics func number", arrayReverse([1,2,3,4,5]));
console.log("generics func string", arrayReverse<string>(["a","b","c","d","e"]));


/** array of object */

type obj = {key:string, value: string};
type objArray = obj[];
const ab: objArray = [{key: "rezaul", value: "haque"}]
console.log("array of object", ab);


/** generics array of object */

type objGen<T> = {key: T, value: T}
type objArrayGen<T> = objGen<T>[];
const abc: objArrayGen<number> = [{key: 8, value: 9}]
const xyz: objArrayGen<string> = [{key: "rezaul", value: "haque"}]
console.log("generics array of object (number)", abc);

/** unknown key array of object */

type unknownKey = {[key: string] : string} // value string can be any if both key and value is unknown
type unKnownKeyArray = unknownKey[];
const unknownKeyArray : unKnownKeyArray = [{name: "rezaul", zzz: "string"}]
console.log("unknown key array of object", unknownKeyArray);

/** generics unknown key object */

type unknownKeyValue<T> = {[key:string]: T}
type unknownKeyValueArray<T> =  unknownKeyValue<T>[]
const def : unknownKeyValueArray<string>  = [{name: "rezaul"}]
const ghi : unknownKeyValueArray<number>  = [{name: 1}]
console.log("generics unknown key object (string)", def);
console.log("generics unknown key object (number)", ghi);

/** generics interface */

interface  ObjInterface<T> {
    propsOne: T
    propsTwo: T
}
const objOne : ObjInterface<number> ={
    propsOne: 1,
    propsTwo: 0
}
const objTwo : ObjInterface<string> = {
    propsOne: "hello",
    propsTwo: "world"
}

/** interface with Union, optional  */
interface PayLoad {
    name: string,
    email: string
    dob?: Date | string
}

interface State {
    submit: (payload: PayLoad) => void
} 

interface FormInterface extends PayLoad, State {
    isLoading: boolean
}

const submitHandler = (actions: FormInterface) : void => {
     console.log("name -->", actions.name);
     console.log("name", actions.email);
     if(actions.dob){
        console.log("dob",actions.dob);
     }
     console.log("submit action", actions.submit);   
}

submitHandler({name:"rezaul",email:"example@example.com",isLoading:true, submit:()=>{}});